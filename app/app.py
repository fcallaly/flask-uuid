from flask import Flask
import uuid

app = Flask(__name__)
app_id = str(uuid.uuid4())

app_version = "v2"

result = ("<html><body><table>" +
          "<thead><th>App Version</th><th>App UUID</th><th>page loads</th></thead>" +
          "<tbody><tr><td>" + app_version + "</td><td>" + app_id + "</td>" +
          "<td>{count:n}</td></tr>" +
          "</tbody></table></body></html>")

count = -1

@app.route("/")
def get_version_uuid():
    global count
    count += 1
    return result.format(count=count)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
